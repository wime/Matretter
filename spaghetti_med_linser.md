# Speghetti med linser

## Ingredienser

* [ ] 280g spaghetti
* [ ] 300g røde linser
* [ ] 3 stk. løk
* [ ] 6 fedd hvitløk
* [ ] 1 stk. chili
* [ ] 2 bokser hakkede tomater
* [ ] 1ss tomatpuré
* [ ] vann
* [ ] 1ss basilikum
* [ ] 1ss oregano
* [ ] 2ts timian
* [ ] 2 stk laurbærblad
* [ ] salt
* [ ] 2ss eddik
* [ ] 1 håndfull Cahsew nøtter

## Tilberedning

1. Kok spaghetti.
2. Skyll linsene godt og sett de i varmt vann. 
3. Kutt opp løk, hvitløk og chili og stek i ca. 10 minutter i en stor gryte.
4. Tilsett tomatpuré og la det steke i ca. 30 sekunder. Rør godt!
5. Hell av linsene og tilsett de i gryten sammen med hakkede tomater, vann og krydder. La det koke i ca. 20 minutter.
6. Mot slutten av koketiden, tilsett eddik etter smak. 
7. Til slutt, rør inn spaghetti og hakkede cashew nøtter. 
