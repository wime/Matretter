# Stekt Ris

## Ingredienser
* [ ] 280g ris
* [ ] reker
* [ ] 6 egg
* [ ] 1 pakker *amerikansk blanding*
* [ ] 2stk løk
* [ ] 4 fedd hvitløk
* [ ] 1 chili
* [ ] 3ss soyasaus
* [ ] 1ss paprika krydder
* [ ] 1ts gurkemeie
* [ ] 1ts sort pepper

## Fremgangsmåte
1. Stek reker i en wok/stekepanne.
2. Kutt opp løk, hvitløk og chili og stek i en stor wok.
3. Etter ca. 10 minutter tilsett grønnskasblandingen og stek frem til alt er tint og stekt.
4. Flytt innholdet i woken til sidene og tilsett vispede egg i midten. La det stå litt før 
du begynner å røre. Rør så litt kontinuerlig og bland egg med grønnsaker.
5. Tilsett (ferdigkokt) ris, krydder og soyasaus og bland alt sammen. Stek noen minutter til før 
alt er ferdig.