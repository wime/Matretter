# Pesto

## Ingredienser

### Kikerter
* [ ] 120g kikerter
* [ ] salt
* [ ] olje
* [ ] basilikum
* [ ] paprika krydder

### Laks
* [ ] 400g Laks
* [ ] olje
* [ ] salt
* [ ] pepper

### Pasta med pesto
* [ ] 280g pasta
* [ ] 120g pesto
* [ ] 200g cherrytomater
* [ ] 2 stk. løk
* [ ] 5 fedd hvitløk
* [ ] 1 stk. chili
* [ ] 1 stk. squash

## Fremgangsmåte

### Kikerter
1. Bland ferdigkokte kikerter med litt olje, salt og krydder.
2. Stek i stekeovn i 20 minutter.

### Laks
1. Stek i stekeovn i ca. 20 minutter sammen med litt olje, salt og pepper.

### Pasta med pesto
1. Kok pasta i ca. 12 minutter.
2. Kutt opp løk, hvitløk og chili og stek i en stor stekepanne.
3. Kutt cherrytomater i to eller fire biter avhengig av størrelse. Kutt også opp squash i skiver. 
4. Etter ca. 10 miutter steking tilsett cherrytomater i pannen. Stek i ca. 5 minutter.
5. Tilsett pesto og squash stek noen minutter til. 
6. Til slutt bland oppi pasta.

### Sett sammen
1. Server pasta, kikerter og laks sammen.
