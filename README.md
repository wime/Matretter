# Matretter

## Fisk
* Laks og pesto
* Torsk curry
* Stekt ris
* Fisk og grønnsaker

## Bønner
* Chili
* Taco
* Bønner og ris
* Salat

## Kikerter
* Kikkerter med Tahini
* Indisk


## Linser
* Etiopisk linsegryte (Misir Wot)
* Linse curry
* Bygg og linser
* Spaghetti med linser
* Linse- og squashsuppe

## Annet
* Pizza
* Wok med Tofu
* Lasagne med soy chunks
* Gresskarsuppe
* Falafel
