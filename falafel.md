# Falafel

## Ingredienser

* [ ] 300g kikerter
* [ ] 1stk løk
* [ ] 8 fedd hvitløk
* [ ] 1ss sort pepper
* [ ] 1ss spisskummen
* [ ] 1ts cayenne
* [ ] 1ts bakepulver

## Tilberedning
1. Sett kikerter i vann over natten, men ikke kok de.
2. Ha kikerter, urter, løk, hvitløk og krydder i en foodprocessor og kjør til en deig.
3. Overfør deigen til en kontainer og sett i kjøleskapet i minste én time.
4. Bland inn bakepulver og fordel i små baller.
5. Stek i stekeovn i ca. 10 mintter på 200 grader.
