# Bønne burger

## Ingredienser
* [ ] 300g kidney bønner
* [ ] 1 stk. egg
* [ ] 1/2 stk. løk
* [ ] 2 fedd hvitløk
* [ ] 1 stk. gulrot
* [ ] 2ss soyasaus
* [ ] 2dL havregryn
* [ ] 1ts spisskummen
* [ ] 1ts sort pepepr
* [ ] 1ts salt
* [ ] koriander

## Tilberedning
1. Kutt opp gulrot og løk i små biter.
2. Kjør alle ingrediensene sammen i en foodprocessor til en deig.
3. Form til burgere og stek i ca. 20minyyer på 200 grader.

[Inspirasjon](https://beritnordstrand.no/oppskrifter/sort-bonneburger)