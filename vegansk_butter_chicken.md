# Vegansk "Butter Chicken"

## Ingredienser
* [ ] 280g ris
* [ ] 300g (tørkede) kikerter
* [ ] 100g tomatpuré
* [ ] 1 boks kokosmelk
* [ ] 2 stk. løk
* [ ] 4 fedd hvitløk
* [ ] Ingefær
* [ ] 1 stk. chili
* [ ] 1 stk. blomkål
* [ ] gurkemeie
* [ ] curry
* [ ] garam masala

## Fremgangsmåte
1. Tilbered ris.
2. Kutt opp blomkål, bland med litt olje og stek i stekeovn i ca. 20 minutter.
3. Kutt opp løk, hvitløk, ingefær og chili og stek i en stor gryte eller wok.
4. Tilsett krydder etter ca. 10 minutter og stek i ca. 30 sekunder. Rør godt!
5. Tilsett tomatpuré og stek i ca. 1 minutt. 
6. Tilsett kokosmelk og la det koke i ca. 10 minutter. Tilsett eventuelt andre grønnsaker i denne perioden avhengig av koketid.
7. Mot slutten tilsett ferdigkokte kikerter. 
8. Server alt sammen.

[Inspirasjon](https://www.noracooks.com/vegan-butter-chicken/)
