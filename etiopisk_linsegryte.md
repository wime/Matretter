# Etiopisk linsegryte (Misir Wot)

## Ingredienser
* [ ] 350g røde linser
* [ ] 2 bokser hakkede tomat
* [ ] 3 stk. løk
* [ ] 6 fedd hvitløk
* [ ] ingefær
* [ ] 3ss berber krydderblanding
* [ ] kokt vann

## Tilberedning
1. Skyll linsene godt og sett de i varmt vann.
2. Kutt opp løk, hvitløk og ingefær og stek i ca. 10 minutter i en stor gryte.
3. Tilsett 2ss av krydderblandingen.
4. Hell av linsene og tilsett i gryten sammen med hakkede tomat og kokt vann. La det koke i ca. 20-30 minutter.
5. Tilsett resten av krydderblandingen og la det koke i 1 minutt til.
6. Server med ris og stekte grønnsaker.