# Wok med Tofu

## Ingredienser

### Tofu
* [ ] Tofu
* [ ] Soyasaus
* [ ] Røykt paprika
* [ ] Chilikrydder
* [ ] Hvitløkspulver
* [ ] Løkpulver
* [ ] Miasstivelse

### Woksaus
* [ ] Soyasaus
* [ ] Chilisaus (f.eks. Sriracha)
* [ ] Eddik
* [ ] Sukker
* [ ] Miasstivelse

### Wok
* [ ] Nudler
* [ ] 2 stk. løk
* [ ] 3 fedd hvitløk
* [ ] 2 stk. paprika
* [ ] 1 stk. gulrot
* [ ] 1 pose grønne bønner
* [ ] 1 håndfull cashew nøtter
* *Eller andre grønnsaker*

## Tilberedning

### Tofu
1. Kutt tofu opp i terninger.
2. Bland med soyasaus og krydder og la stå i minst 10 minutter (helst lenger).
3. Bland inn maisstvelse og stek i air-fryer i ca. 20 minutter. 

### Woksaus
1. Bland sammen alle ingredienser til wokasausen.

### Wok
1. Kok nudler.
2. Kutt opp og stek løk og hvitløk i en stor wok i 5-10 minutter.
3. Kutt opp grønnsaker (paprika og gulrøtter). 
4. Tilsett *ikke-frosne* grønnsaker og stek i 1 minutt til.
5. Tilsett frosne grønnsaker (grønne bønner) og stek i 30 sekudner til. 
6. Tilsett saus og ferdigkokte nudler og stek/kok i 1 minutt til. Rør godt for å unngå at nudlene brenner fast.
7. Tilsett cashew nøtter og tofu.
